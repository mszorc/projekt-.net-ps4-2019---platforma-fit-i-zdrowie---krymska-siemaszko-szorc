ProjektFit:

ProjektFit is a platform where users can join groups, use already made or make their own
training plans, use BMI & BMR calculators and store their measurements.

Authors:

* Adrianna Justyna Krymska
* Izabela Siemaszko
* Michał Szorc

Website:
https://projektfit20190611104536.azurewebsites.net/

Address to repository:

https://gitlab.com/mszorc/projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc

How to install:

Download whole package and open ProjektFit.sln file in Visual Studio 2017. Press ctrl+f5 to compile

ProjektFit is based on .NET Core 2.2 technology.
Database: SQL Server 14.0.3048.4

Used libraries:
* HighCharts v1.0.0
* Sendgird v9.11.0
* Syncfusion.Pdf.Net.Core v17.1.0.51


Application was made and tested on Windows 10.

Test users:
*  login: admin1@email.com  password: P@$$w0rd (Admin role)
*  login: admin2@email.com  password: P@$$w0rd (Admin role)
*  login: user1@email.com  password: P@$$w0rd (User role)
*  login: user2@email.com  password: P@$$w0rd (User role)