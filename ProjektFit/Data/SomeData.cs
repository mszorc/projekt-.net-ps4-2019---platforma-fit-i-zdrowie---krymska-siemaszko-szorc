﻿using Microsoft.AspNetCore.Identity;
using ProjektFit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektFit.Data
{
    public class SomeData
    {
        public static async Task Initialize(ApplicationDbContext context,
                              UserManager<ApplicationUser> userManager,
                              RoleManager<ApplicationRole> roleManager)
        {
            context.Database.EnsureCreated();

            String adminId1 = "";
            String adminId2 = "";

            string role1 = "Admin";
            string desc1 = "This is the administrator role";

            string role2 = "User";
            string desc2 = "This is the users role";

            string password = "P@$$w0rd";

            if (await roleManager.FindByNameAsync(role1) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role1, desc1));
            }
            if (await roleManager.FindByNameAsync(role2) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role2, desc2));
            }

            if (await userManager.FindByNameAsync("admin1@email.com") == null)
            {
                var user = new ApplicationUser
                {
                    UserName = "admin1@email.com",
                    Email = "admin1@email.com",
                    FirstName = "Jan",
                    LastName = "Kowalski",
                    City = "Białystok",
                    Country = "Polska"
                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role1);
                }
                adminId1 = user.Id;
            }

            if (await userManager.FindByNameAsync("admin2@email.com") == null)
            {
                var user = new ApplicationUser
                {
                    UserName = "admin2@email.com",
                    Email = "admin2@email.com",
                    FirstName = "Piotr",
                    LastName = "Nowak",
                    City = "Białystok",
                    Country = "Polska"
                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role1);
                }
                adminId2 = user.Id;
            }

            if (await userManager.FindByNameAsync("user1@email.com") == null)
            {
                var user = new ApplicationUser
                {
                    UserName = "user1@email.com",
                    Email = "user1@email.com",
                    FirstName = "Michał",
                    LastName = "Szorc",
                    City = "Białystok",
                    Country = "Polska"
                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role2);
                }
            }

            if (await userManager.FindByNameAsync("user2@email.com") == null)
            {
                var user = new ApplicationUser
                {
                    UserName = "user2@email.com",
                    Email = "user2@email.com",
                    FirstName = "Michał",
                    LastName = "Szorc",
                    City = "Białystok",
                    Country = "Polska"
                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role2);
                }
            }
        }
    }
}
