#pragma checksum "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "24d916974452ae380157254de8eec299a70699e9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Groups_Details), @"mvc.1.0.view", @"/Views/Groups/Details.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Groups/Details.cshtml", typeof(AspNetCore.Views_Groups_Details))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\_ViewImports.cshtml"
using ProjektFit;

#line default
#line hidden
#line 2 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\_ViewImports.cshtml"
using ProjektFit.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"24d916974452ae380157254de8eec299a70699e9", @"/Views/Groups/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ebba460622bcf839d9fbdb3ca2b36690f314e860", @"/Views/_ViewImports.cshtml")]
    public class Views_Groups_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ProjektFit.Models.Group>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddUser", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "RemoveUser", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "IndexPrivate", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
  
    ViewData["Title"] = "Details";

#line default
#line hidden
            BeginContext(75, 23, true);
            WriteLiteral("<h2>Details</h2>\r\n<p>\r\n");
            EndContext();
#line 7 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
     if (ViewBag.Group == 1)
    {

#line default
#line hidden
            BeginContext(135, 17, true);
            WriteLiteral("    <p>\r\n        ");
            EndContext();
            BeginContext(152, 85, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24d916974452ae380157254de8eec299a70699e95538", async() => {
                BeginContext(203, 30, true);
                WriteLiteral("<font color=\"navy\">Edit</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 10 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
                               WriteLiteral(Model.GroupId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(237, 29, true);
            WriteLiteral("\r\n    </p>\r\n    <p>\r\n        ");
            EndContext();
            BeginContext(266, 88, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24d916974452ae380157254de8eec299a70699e97991", async() => {
                BeginContext(319, 31, true);
                WriteLiteral("<font color=\"red\">Delete</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 13 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
                                 WriteLiteral(Model.GroupId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(354, 29, true);
            WriteLiteral("\r\n    </p>\r\n    <p>\r\n        ");
            EndContext();
            BeginContext(383, 107, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24d916974452ae380157254de8eec299a70699e910448", async() => {
                BeginContext(437, 49, true);
                WriteLiteral("<font color=\"teal\">Add a User to the group</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 16 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
                                  WriteLiteral(Model.GroupId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(490, 12, true);
            WriteLiteral("\r\n    </p>\r\n");
            EndContext();
#line 18 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
}

#line default
#line hidden
            BeginContext(505, 4, true);
            WriteLiteral("    ");
            EndContext();
#line 19 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
     if (ViewBag.Group == 2)
    {

#line default
#line hidden
            BeginContext(542, 25, true);
            WriteLiteral("        <p>\r\n            ");
            EndContext();
            BeginContext(567, 107, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24d916974452ae380157254de8eec299a70699e913564", async() => {
                BeginContext(621, 49, true);
                WriteLiteral("<font color=\"teal\">Add a User to the group</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 22 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
                                      WriteLiteral(Model.GroupId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(674, 16, true);
            WriteLiteral("\r\n        </p>\r\n");
            EndContext();
#line 24 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
    }

#line default
#line hidden
            BeginContext(697, 116, true);
            WriteLiteral("\r\n    </p>\r\n    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>\r\n                    ");
            EndContext();
            BeginContext(814, 40, false);
#line 31 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.Type));

#line default
#line hidden
            EndContext();
            BeginContext(854, 67, true);
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
            EndContext();
            BeginContext(922, 40, false);
#line 34 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.Name));

#line default
#line hidden
            EndContext();
            BeginContext(962, 67, true);
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
            EndContext();
            BeginContext(1030, 40, false);
#line 37 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.Date));

#line default
#line hidden
            EndContext();
            BeginContext(1070, 67, true);
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
            EndContext();
            BeginContext(1138, 41, false);
#line 40 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.Place));

#line default
#line hidden
            EndContext();
            BeginContext(1179, 168, true);
            WriteLiteral("\r\n                </th>\r\n                <th></th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n\r\n            <tr>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(1348, 36, false);
#line 49 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayFor(model => model.Type));

#line default
#line hidden
            EndContext();
            BeginContext(1384, 67, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(1452, 36, false);
#line 52 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayFor(model => model.Name));

#line default
#line hidden
            EndContext();
            BeginContext(1488, 67, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(1556, 36, false);
#line 55 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayFor(model => model.Date));

#line default
#line hidden
            EndContext();
            BeginContext(1592, 67, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(1660, 37, false);
#line 58 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
               Write(Html.DisplayFor(model => model.Place));

#line default
#line hidden
            EndContext();
            BeginContext(1697, 69, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1766, "\"", 1829, 2);
            WriteAttributeValue("", 1773, "/Groups/Members/", 1773, 16, true);
#line 61 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
WriteAttributeValue("", 1789, Html.DisplayFor(model => model.GroupId), 1789, 40, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1830, 40, true);
            WriteLiteral("><font color=\"navy\">Members</font></a>\r\n");
            EndContext();
#line 62 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
                     if (ViewBag.AbleToModify == true)
                    {
                        

#line default
#line hidden
            BeginContext(1979, 3, true);
            WriteLiteral(" | ");
            EndContext();
            BeginContext(1989, 26, true);
            WriteLiteral("\r\n                        ");
            EndContext();
            BeginContext(2015, 92, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24d916974452ae380157254de8eec299a70699e921741", async() => {
                BeginContext(2072, 31, true);
                WriteLiteral("<font color=\"red\">Remove</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 65 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
                                                     WriteLiteral(Model.GroupId);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2107, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 66 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
                    }

#line default
#line hidden
            BeginContext(2132, 165, true);
            WriteLiteral("                </td>\r\n            </tr>\r\n\r\n        </tbody>\r\n    </table>\r\n    <p>\r\n        <dt>\r\n            Owner Group\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(2298, 13, false);
#line 77 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Details.cshtml"
       Write(ViewBag.Owner);

#line default
#line hidden
            EndContext();
            BeginContext(2311, 44, true);
            WriteLiteral("\r\n        </dd>\r\n    </p>\r\n    <p>\r\n        ");
            EndContext();
            BeginContext(2355, 72, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24d916974452ae380157254de8eec299a70699e925141", async() => {
                BeginContext(2384, 39, true);
                WriteLiteral("<font color=\"black\">Back to List</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2427, 12, true);
            WriteLiteral("\r\n    </p>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ProjektFit.Models.Group> Html { get; private set; }
    }
}
#pragma warning restore 1591
