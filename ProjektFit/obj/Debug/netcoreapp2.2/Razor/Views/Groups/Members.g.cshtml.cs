#pragma checksum "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2052fe752a2d0fd96168e3e67707f18377592443"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Groups_Members), @"mvc.1.0.view", @"/Views/Groups/Members.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Groups/Members.cshtml", typeof(AspNetCore.Views_Groups_Members))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\_ViewImports.cshtml"
using ProjektFit;

#line default
#line hidden
#line 2 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\_ViewImports.cshtml"
using ProjektFit.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2052fe752a2d0fd96168e3e67707f18377592443", @"/Views/Groups/Members.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ebba460622bcf839d9fbdb3ca2b36690f314e860", @"/Views/_ViewImports.cshtml")]
    public class Views_Groups_Members : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<ProjektFit.Models.GroupModel>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "RemoveUser", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Details", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
  
    ViewData["Title"] = "Members";

#line default
#line hidden
            BeginContext(93, 102, true);
            WriteLiteral("<h1>Members</h1>\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(196, 44, false);
#line 10 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
           Write(Html.DisplayNameFor(model => model.UserName));

#line default
#line hidden
            EndContext();
            BeginContext(240, 86, true);
            WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
            EndContext();
#line 16 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
         if (ViewBag.AbleToModify == true)
        {
            

#line default
#line hidden
#line 18 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
            BeginContext(438, 72, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(511, 43, false);
#line 22 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
                   Write(Html.DisplayFor(modelItem => item.UserName));

#line default
#line hidden
            EndContext();
            BeginContext(554, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(633, 86, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2052fe752a2d0fd96168e3e67707f183775924436126", async() => {
                BeginContext(684, 31, true);
                WriteLiteral("<font color=\"red\">Remove</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 25 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
                                                     WriteLiteral(item.ID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(719, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 28 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
            }

#line default
#line hidden
#line 28 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
             
        }
        else
        {
            

#line default
#line hidden
#line 32 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
            BeginContext(879, 72, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(952, 43, false);
#line 36 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
                   Write(Html.DisplayFor(modelItem => item.UserName));

#line default
#line hidden
            EndContext();
            BeginContext(995, 29, true);
            WriteLiteral("\r\n                    </td>\r\n");
            EndContext();
#line 38 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
                     if (ViewBag.User == item.UserId)
                    {

#line default
#line hidden
            BeginContext(1102, 58, true);
            WriteLiteral("                        <td>\r\n                            ");
            EndContext();
            BeginContext(1160, 86, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2052fe752a2d0fd96168e3e67707f1837759244310417", async() => {
                BeginContext(1211, 31, true);
                WriteLiteral("<font color=\"red\">Remove</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 41 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
                                                         WriteLiteral(item.ID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1246, 33, true);
            WriteLiteral("\r\n                        </td>\r\n");
            EndContext();
#line 43 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
                    }

#line default
#line hidden
            BeginContext(1302, 23, true);
            WriteLiteral("                </tr>\r\n");
            EndContext();
#line 45 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
            }

#line default
#line hidden
#line 45 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
             
        }

#line default
#line hidden
            BeginContext(1351, 33, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n<p>\r\n    ");
            EndContext();
            BeginContext(1384, 95, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2052fe752a2d0fd96168e3e67707f1837759244313809", async() => {
                BeginContext(1435, 40, true);
                WriteLiteral("<font color=\"black\">Back to Group</font>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 50 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
                              WriteLiteral(ViewBag.ID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1479, 8, true);
            WriteLiteral("\r\n</p>\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(1505, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 53 "C:\Users\szorc\Desktop\Projekt koniec\projekt-.net-ps4-2019---platforma-fit-i-zdrowie---krymska-siemaszko-szorc\ProjektFit\Views\Groups\Members.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<ProjektFit.Models.GroupModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
