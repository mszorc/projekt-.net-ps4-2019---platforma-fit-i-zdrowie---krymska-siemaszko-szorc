﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace ProjektFit.Models
{
    public partial class TrainingPlan
    {
        public TrainingPlan()
        {
            ExerciseTrainingPlan = new HashSet<ExerciseTrainingPlan>();
        }

        public int TrainingPlanId { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        public string OwnerId { get; set; }

        public virtual ICollection<ExerciseTrainingPlan> ExerciseTrainingPlan { get; set; }
    }
}
