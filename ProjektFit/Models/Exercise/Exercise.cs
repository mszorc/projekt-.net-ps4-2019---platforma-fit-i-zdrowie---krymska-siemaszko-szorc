﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjektFit.Models
{
    public partial class Exercise
    {
        public Exercise()
        {
            ExerciseTrainingPlan = new HashSet<ExerciseTrainingPlan>();
        }

        public int ExerciseId { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        [Range(1,1000)]
        public int Calories { get; set; }
        public string OwnerId { get; set; }

        public virtual ICollection<ExerciseTrainingPlan> ExerciseTrainingPlan { get; set; }
    }
}
