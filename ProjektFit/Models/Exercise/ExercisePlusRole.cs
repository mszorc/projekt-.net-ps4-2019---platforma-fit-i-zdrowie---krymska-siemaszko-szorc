﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektFit.Models
{
    public class ExercisePlusRole
    {
        public int ExerciseId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int Calories { get; set; }
        public string OwnerId { get; set; }
        public string RoleName { get; set; }
        public bool AbleToModify { get; set; }

        public ExercisePlusRole(Exercise exercise, string rolename, bool abletomodify)
        {
            ExerciseId = exercise.ExerciseId;
            Name = exercise.Name;
            Url = exercise.Url;
            Calories = exercise.Calories;
            OwnerId = exercise.OwnerId;
            RoleName = rolename;
            AbleToModify = abletomodify;
        }
    }
}
