﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektFit.Models
{
    public class TrainingPlanPlusETPID
    {
        public int ID { get; set; }
        public int ExerciseId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int Calories { get; set; }

        public TrainingPlanPlusETPID(Exercise exercise, ExerciseTrainingPlan exercisetrainingplan)
        {
            ExerciseId = exercise.ExerciseId;
            Name = exercise.Name;
            Url = exercise.Url;
            Calories = exercise.Calories;
            ID = exercisetrainingplan.Id;
        }
    }
}
