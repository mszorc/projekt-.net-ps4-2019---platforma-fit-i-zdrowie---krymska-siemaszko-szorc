﻿using System;
using System.Collections.Generic;

namespace ProjektFit.Models
{
    public partial class ExerciseTrainingPlan
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public int TrainingPlanId { get; set; }
        public string OwnerId { get; set; }

        public virtual Exercise Exercise { get; set; }
        public virtual TrainingPlan TrainingPlan { get; set; }
    }
}
