﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ProjektFit.Models
{
    public partial class projectfitContext : DbContext
    {
        public projectfitContext()
        {
        }

        public projectfitContext(DbContextOptions<projectfitContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Exercise> Exercise { get; set; }
        public virtual DbSet<ExerciseTrainingPlan> ExerciseTrainingPlan { get; set; }
        public virtual DbSet<TrainingPlan> TrainingPlan { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("DefaultConnection");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<Exercise>(entity =>
            {
                entity.Property(e => e.ExerciseId).HasColumnName("ExerciseID");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasColumnName("OwnerID");

                entity.Property(e => e.Url).IsRequired();
            });

            modelBuilder.Entity<ExerciseTrainingPlan>(entity =>
            {
                entity.ToTable("Exercise_TrainingPlan");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ExerciseId).HasColumnName("ExerciseID");

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasColumnName("OwnerID");

                entity.Property(e => e.TrainingPlanId).HasColumnName("TrainingPlanID");

                entity.HasOne(d => d.Exercise)
                    .WithMany(p => p.ExerciseTrainingPlan)
                    .HasForeignKey(d => d.ExerciseId)
                    .HasConstraintName("FK__Exercise___Exerc__4F7CD00D");

                entity.HasOne(d => d.TrainingPlan)
                    .WithMany(p => p.ExerciseTrainingPlan)
                    .HasForeignKey(d => d.TrainingPlanId)
                    .HasConstraintName("FK__Exercise___Train__5070F446");
            });

            modelBuilder.Entity<TrainingPlan>(entity =>
            {
                entity.Property(e => e.TrainingPlanId).HasColumnName("TrainingPlanID");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasColumnName("OwnerID");
            });
        }
    }
}
