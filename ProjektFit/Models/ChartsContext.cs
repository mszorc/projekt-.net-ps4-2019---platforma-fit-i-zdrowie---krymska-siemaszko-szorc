﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ProjektFit.Models
{
    public partial class ChartsContext : DbContext
    {
        public ChartsContext()
        {
        }

        public ChartsContext(DbContextOptions<ChartsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Charts> Charts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("DefaultConnection");
            }
        }

        internal static IEnumerable<object> Where(Func<object, object> p)
        {
            throw new NotImplementedException();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Charts>(entity =>
            {
                entity.Property(e => e.ObwodWKlatcePiersiowej).HasColumnName("Obwod_w_klatce_piersiowej");

                entity.Property(e => e.ObwodWPasie).HasColumnName("Obwod_w_pasie");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);
            });
        }
    }
}
