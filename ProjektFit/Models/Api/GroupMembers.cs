﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektFit.Models.Api
{
    public class GroupMembers
    {
        public int GroupId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string Place { get; set; }
        public string OwnerGroupId { get; set; }
        public int NumberOfMembers { get; set; }

        public GroupMembers()
        {

        }

        public GroupMembers(Group group, int numberofmembers)
        {
            GroupId = group.GroupId;
            Type = group.Type;
            Name = group.Name;
            Date = group.Date;
            Place = group.Place;
            OwnerGroupId = group.OwnerGroupId;
            NumberOfMembers = numberofmembers;
        }
    }
}
