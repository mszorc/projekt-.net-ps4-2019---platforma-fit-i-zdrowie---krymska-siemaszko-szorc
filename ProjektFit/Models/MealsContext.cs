﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ProjektFit.Models
{
    public partial class MealsContext : DbContext
    {
        public MealsContext()
        {
        }

        public MealsContext(DbContextOptions<MealsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Meals> Meals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("DefaultConnection");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Meals>(entity =>
            {
                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Typeofday).IsRequired();

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);
            });
        }
    }
}
