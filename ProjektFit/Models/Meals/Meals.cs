﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjektFit.Models
{
    public partial class Meals
    {
        public int MealsId { get; set; }
        [Required]
        public string Typeofday { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        [Required]
        [Range(1,2000)]
        public int Calories { get; set; }
        [Required]
        [Range(1, 500)]
        public int Protein { get; set; }
        [Required]
        [Range(1, 500)]
        public int Carbohydres { get; set; }
        [Required]
        [Range(1, 500)]
        public int Fats { get; set; }
        public string UserId { get; set; }
    }
}
