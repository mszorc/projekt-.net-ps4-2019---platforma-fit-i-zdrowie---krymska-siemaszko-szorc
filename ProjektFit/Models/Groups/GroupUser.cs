﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjektFit.Models
{
    public partial class GroupUser
    {
     

        public int GroupUserId { get; set; }
        [Required]
        public int GroupId { get; set; }
        [Required]
        public string UserId { get; set; }

        public virtual Group Group { get; set; }


    }
}
