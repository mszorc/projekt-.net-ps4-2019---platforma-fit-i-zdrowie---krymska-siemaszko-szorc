﻿using System;
using System.Collections;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ProjektFit.Models
{
    public partial class GroupsContext : DbContext
    {
        public GroupsContext()
        {
        }

        public GroupsContext(DbContextOptions<GroupsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupUser> GroupUser { get; set; }
        public IEnumerable ApplicationDbContext { get; internal set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("DefaultConnection");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.Date).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.OwnerGroupId).IsRequired();

                entity.Property(e => e.Place).IsRequired();

                entity.Property(e => e.Type).IsRequired();
            });

            modelBuilder.Entity<GroupUser>(entity =>
            {
                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupUser)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK__GroupUser__Group__10566F31");
            });
        }
    }
}
