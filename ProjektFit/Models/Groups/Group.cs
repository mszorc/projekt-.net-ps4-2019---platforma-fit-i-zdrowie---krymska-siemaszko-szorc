﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjektFit.Models
{
    public partial class Group
    {
        public Group()
        {
            GroupUser = new HashSet<GroupUser>();
        }

        public int GroupId { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        [StringLength(20)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Date { get; set; }
        [Required]
        [StringLength(40)]
        public string Place { get; set; }
        public string OwnerGroupId { get; set; }

        public virtual ICollection<GroupUser> GroupUser { get; set; }
    }
}
