﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektFit.Models
{
    public class GroupModel
    {

        public int ID { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }

        public GroupModel(ApplicationUser group, GroupUser item)
        {
            UserId = group.Id;
            UserName = group.UserName;
            ID = item.GroupUserId;
        }
    }
}
























