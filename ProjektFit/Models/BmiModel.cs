﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace ProjektFit.Models
{

    public class BmiModel
    {

        public string Wiadomosc { get; set; }

        [Required]
        [Range(100, 250)]
        [Display(Name = "Height (cm)")]
        public int Wzrost { get; set; }

        [Required]
        [Range(25,300)]
        [Display(Name = "Weight (kg)")]
        public int Waga { get; set; }
      
    }
}
