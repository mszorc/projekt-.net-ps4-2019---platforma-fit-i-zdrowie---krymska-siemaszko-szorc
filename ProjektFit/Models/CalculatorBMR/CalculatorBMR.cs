﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjektFit.Models
{
    public class CalculatorBMR
    {
      
        [Required]
        public Genderr Gender { get; set; }
        [Required]
        [Range(1, 90)]
        public int Age { get; set; }
        [Required]
        [Range(25, 300)]
        public int Weight { get; set; }
        [Required]
        [Range(100, 250)]
        public int Height { get; set; }
        [Required]
        public Activityy Activity { get; set; }

        public enum Genderr
        {
            Female,
            Male
        }

        public enum Activityy
        {
            Negligible, 
            VerySmall,
            Little,
            Moderate,
            Big,
            VeryBig
        }
    }
}
