﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjektFit.Models
{
    public partial class Charts
    {

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime ExpenseDate { get; set; } = DateTime.Now;

        public int ChartsId { get; set; }
        public string UserId { get; set; }

        [Required]
        [Range(25, 300)]
        [Display(Name = "Weight (kg)")]
        public int Waga { get; set; }

        [Required]
        [Range(40, 200)]
        [Display(Name = "Chest circumference (cm)")]
        public int ObwodWKlatcePiersiowej { get; set; }

        [Required]
        [Range(40, 250)]
        [Display(Name = "Waist circumference (cm)")]
        public int ObwodWPasie { get; set; }
    }
  

}
