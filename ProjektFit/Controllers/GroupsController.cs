﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjektFit.Data;
using ProjektFit.Models;

namespace ProjektFit.Controllers
{
  
    public class GroupsController : Controller
    {
        private readonly GroupsContext _context;
        private readonly ApplicationDbContext _contextUser;
        protected IAuthorizationService AuthorizationService { get; }


        public GroupsController(GroupsContext context,
            ApplicationDbContext contextUser, IAuthorizationService authorizationService)
        {
            _context = context;
            _contextUser = contextUser;
            AuthorizationService = authorizationService;
        }
      

        // GET: Groups
        public async Task<IActionResult> Index()
        {
            var groups = from c in _context.Group select c;
            if (groups == null)
            {
                return NotFound();
            }
            return View(groups);
        }

        [Authorize]
        public async Task<IActionResult> IndexPrivate()
        {
            var group = from c in _context.Group select c;
            if (group == null)
            {
                return NotFound();
            }
            return View(group);
        }

        [Authorize]
        public async Task<IActionResult> Members(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewBag.ID = id;
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;//Id osoby zalogowanej
            var group = await _context.Group
              .FirstOrDefaultAsync(m => m.GroupId == id);
            if (group == null)
            {
                return NotFound();
            }
            ViewBag.User = currentUserId;
            if (group.OwnerGroupId == currentUserId || HttpContext.User.IsInRole("Admin"))
            {
                ViewBag.AbleToModify = true;
            }
            else
            {
                ViewBag.AbleToModify = false;
            }
            var groups = await _context.GroupUser
              .Include(e => e.Group)
              .FirstOrDefaultAsync(m => m.GroupUserId == id);
            var temp = _context.GroupUser.Where(m=>m.GroupId==id);
            List<GroupModel> groupuser = new List < GroupModel>();

            foreach (var item in temp)//idzie po kolei po id
            {
                var groupp = await _contextUser.Users.FirstOrDefaultAsync(m => m.Id == item.UserId);
                GroupModel x = new GroupModel(groupp, item);
                groupuser.Add(x);
            }
           

            return View(groupuser);
        }

        //Get: Groups/Members/RemoveUser
        [Authorize]
        public async Task<IActionResult> RemoveUser(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groups = await _context.GroupUser
                .Include(e => e.Group)
                .FirstOrDefaultAsync(m => m.GroupUserId == id);
       
            if (groups == null)
            {
                return NotFound();
            }

            var groupOwner = await _context.Group.FirstOrDefaultAsync(m => m.GroupId == groups.GroupId);
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (groupOwner == null)
            {
                return NotFound();
            }
            if (HttpContext.User.IsInRole("Admin") || groupOwner.OwnerGroupId == currentUserId || groups.UserId == currentUserId)
            {
                return View(groups);
            }
            else
            {
                return NotFound();
            }
        }

        //Get: Groups/Members/RemoveUser/5
        [Authorize]
        [HttpPost, ActionName("RemoveUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveUserConfirmed(int id)
        {
           
            var groupuser = await _context.GroupUser.FindAsync(id);
                _context.GroupUser.Remove(groupuser);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", new { id = groupuser.GroupId});
        }


        // GET: Groups/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;//Id osoby zalogowanej
            var group = await _context.Group
              .FirstOrDefaultAsync(m => m.GroupId == id);

            if (group == null)
            {
                return NotFound();
            }

            var groupss = await _context.GroupUser
             .Include(e => e.Group)
             .FirstOrDefaultAsync(m => m.GroupUserId == id);
          
            if (group.OwnerGroupId == currentUserId && group.Type=="Private")
            {
                ViewBag.Group=1;
            }
            else if(group.OwnerGroupId == currentUserId && group.Type == "Public")
            {
                ViewBag.Group = 1;
            }
            else if(group.OwnerGroupId != currentUserId && group.Type == "Private")
            {
                ViewBag.Group = 2;
            }
            else
            {
                ViewBag.Group = 2;
            }
            var groups = await _context.Group
               .FirstOrDefaultAsync(m => m.GroupId == id);

            var currentItemUserName = await _contextUser.Users
              .FirstOrDefaultAsync(m => m.Id == groups.OwnerGroupId);

            if (currentItemUserName == null) return NotFound();

            ViewBag.Owner = currentItemUserName.UserName;
            //   var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (groups == null)
            {
                return NotFound();
            }
    
         //   if (currentUserId == groups.OwnerGroupId)
           // {
             //   return View(groups);
            //}
           // else return NotFound();
            return View(groups);
        }

        // GET: Groups/Create
        [Authorize]
        public IActionResult Create()
        {
            List<string> temp = new List<string>();
            temp.Add("Public");
            temp.Add("Private");
           
            if (TempData["ModelState"] != null)
            {
                ModelState.AddModelError(string.Empty, (string)TempData["ModelState"]);
            }
            ViewData["Type"] = new SelectList(temp);
            return View();
        }

        // POST: Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GroupId,Type,Name,Date,Place,OwnerGroupId")] Group groups)
        {
            if (ModelState.IsValid)
            {
                groups.OwnerGroupId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                _context.Add(groups);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(IndexPrivate));
            }
            TempData["ModelState"] = "You must fill in all of the fields";
            return RedirectToAction("Create");
        }

        // GET: Groups/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groups = await _context.Group.FindAsync(id);

            TempData["OwnerGroupId"] = groups.OwnerGroupId;
            TempData["Type"] = groups.Type;

            if (groups == null)
            {
                return NotFound();
            }

            var groupOwner = await _context.Group.FirstOrDefaultAsync(m => m.GroupId == groups.GroupId);
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (HttpContext.User.IsInRole("Admin") || groupOwner.OwnerGroupId == currentUserId)
            {
                return View(groups);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("GroupId,Type,Name,Date,Place,OwnerGroupId")] Group groups)
        {
            if (id != groups.GroupId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    groups.OwnerGroupId = (string)TempData["OwnerGroupId"];
                    groups.Type = (string)TempData["Type"];
                    _context.Update(groups);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GroupExists(groups.GroupId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = groups.GroupId });
            }
            return View(groups);
        }

        // GET: Groups/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groups = await _context.Group
               .FirstOrDefaultAsync(m => m.GroupId == id);
            if (groups == null)
            {
                return NotFound();
            }

            var groupOwner = await _context.Group.FirstOrDefaultAsync(m => m.GroupId == id);
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (HttpContext.User.IsInRole("Admin") || groupOwner.OwnerGroupId == currentUserId)
            {
                return View(groups);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: Groups/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var groups = await _context.Group.FindAsync(id);
            _context.Group.Remove(groups);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(IndexPrivate));
        }

        private bool GroupExists(int id)
        {
            return _context.Group.Any(e => e.GroupId == id);
        }

        [Authorize]
        public async Task<IActionResult> AddUser(int? id)
        {
            int pom=0;
            if (TempData["ModelState"] != null)
            {
                ModelState.AddModelError(string.Empty, (string)TempData["ModelState"]);
            }

            var temp = _context.GroupUser.Where(m => m.GroupId == id);
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            foreach (var item in temp)//idzie po kolei po id
            {

                if (item.UserId == currentUserId)//user należy do grupy 
                {
                    pom= 1;
                    ViewData["UserId"] = new SelectList(_contextUser.Users, "Id", "UserName");
                }

            }
            if(pom!=1)
            {
                var temp2 = await _context.GroupUser.FirstOrDefaultAsync(m => m.UserId == currentUserId);
                ViewData["UserId"] = new SelectList(_contextUser.Users.Where(m => m.Id == currentUserId), "Id", "UserName");
            }
            var temp3 = await _context.Group.FirstOrDefaultAsync(m => m.GroupId == id);
            TempData["GroupId"] = temp3.GroupId;

            var groupOwner = await _context.Group.FirstOrDefaultAsync(m => m.GroupId == id);
            
            ViewBag.GroupId = id;
           
            if (groupOwner.Type == "Private")
            {
                if (HttpContext.User.IsInRole("Admin") || groupOwner.OwnerGroupId == currentUserId)
                {
                    return View();
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return View();
            }
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUser([Bind("GroupId,UserId")] GroupUser groupuser)
        {
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (ModelState.IsValid)
            {
                int groupID = (int)TempData["GroupId"];

                var groupuser2 = await _context.GroupUser
                    .FirstOrDefaultAsync(m => m.GroupId == groupID
                    && m.UserId == groupuser.UserId);

                GroupUser user = await _context.GroupUser.FirstOrDefaultAsync(m => m.GroupId == groupuser.GroupId && m.UserId == groupuser.UserId);
                Group owner = await _context.Group.FirstOrDefaultAsync(m => m.OwnerGroupId == groupuser.UserId );
                if (groupuser2!=null)
                {
                    if(owner!=null)
                    {
                        TempData["ModelState"] = "User is the owner of this group.";
                        return RedirectToAction("AddUser");
                    }
                    else
                    {
                        TempData["ModelState"] = "User alredy exists in this group.";
                        return RedirectToAction("AddUser");
                    }
                    
                }
                else
                {
                    groupuser.GroupId = groupID;
                    _context.Add(groupuser);
                    await _context.SaveChangesAsync();
                    ModelState.Clear();
                    return RedirectToAction("Details", new { id = groupuser.GroupId });
                }
             
            
            }
           
            ViewData["UserId"] = new SelectList(_contextUser.Users, "Id", "UserName", groupuser.UserId);
            ViewData["GroupId"] = new SelectList(_context.Group, "GroupId", "Name", groupuser.GroupId);
            return RedirectToAction("Details", new { id = groupuser.GroupId });
        }



    }
}
