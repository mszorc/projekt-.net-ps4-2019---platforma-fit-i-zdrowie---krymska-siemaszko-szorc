﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjektFit.Models;
using ProjektFit.Models.Api;

namespace ProjektFit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupApiController : ControllerBase
    {
        private readonly GroupsContext _context;

        public GroupApiController(GroupsContext context)
        {
            _context = context;
        }

        // GET: api/Api
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupMembers>>> GetGroup()
        {
            var PublicGroups = await _context.Group.Where(m => m.Type == "Public").ToListAsync();

            if (PublicGroups == null)
            {
                return NotFound();
            }

            List<GroupMembers> PublicGroupsMembers = new List<GroupMembers>();

            foreach(var item in PublicGroups)
            {
                GroupMembers temp;
                var ListOfMembers = await _context.GroupUser.Where(m => m.GroupId == item.GroupId).ToListAsync();
                int number = ListOfMembers.Count;
                temp = new GroupMembers(item, number);
                PublicGroupsMembers.Add(temp);
                
            }

            return PublicGroupsMembers;
        }

        private bool GroupExists(int id)
        {
            return _context.Group.Any(e => e.GroupId == id);
        }
    }
}
