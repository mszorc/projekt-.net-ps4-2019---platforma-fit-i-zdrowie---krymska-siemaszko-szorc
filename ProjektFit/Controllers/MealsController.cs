﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjektFit.Models;

namespace ProjektFit.Controllers
{
    [Authorize]
    public class MealsController : Controller
    {
        private readonly List<string> type = new List<string>(new string[] { "Breakfast", "Brunch", "Lunch", "Dinner", "Tea", "Supper"});
        private readonly MealsContext _context;
        protected IAuthorizationService AuthorizationService { get; }

        public MealsController(MealsContext context, IAuthorizationService authorizationService)
        {
            _context = context;
            AuthorizationService = authorizationService;
        }

        // GET: Meals
        public async Task<IActionResult> Index()
        {
            if (HttpContext.User.IsInRole("Admin")) ViewBag.Admin = true;
            else ViewBag.Admin = false;
            return View(await _context.Meals.ToListAsync());
        }

        // GET: Meals/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meals = await _context.Meals
                .FirstOrDefaultAsync(m => m.MealsId == id);
            if (HttpContext.User.IsInRole("Admin"))
            {
                ViewBag.AbleToModify = true;
            }
            else
            {
                ViewBag.AbleToModify = false;
            }
            if (meals == null)
            {
                return NotFound();
            }

            return View(meals);
        }

        // GET: Meals/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            if (TempData["ModelState"] != null)
            {
                ModelState.AddModelError(string.Empty, (string)TempData["ModelState"]);
            }
            ViewData["Type"] = new SelectList(type);
            return View();
        }

        // POST: Meals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Create([Bind("MealsId,Name,Typeofday,Calories,Protein,Carbohydres,Fats,UserId")] Meals meals)
        {
            if (ModelState.IsValid)
            {
                if (HttpContext.User.IsInRole("Admin"))
                {
                    ViewBag.AbleToModify = true;
                }
                else
                {
                    ViewBag.AbleToModify = false;
                }
                meals.UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                _context.Add(meals);
                await _context.SaveChangesAsync();
                ModelState.Clear();
                return RedirectToAction(nameof(Index));
            }
            TempData["ModelState"] = "You must fill in all of the fields";
            return RedirectToAction("Create");
        }

        // GET: Meals/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var meals = await _context.Meals.FindAsync(id);
            ViewData["Type"] = new SelectList(type);
            TempData["UserId"] = meals.UserId;
            if (meals == null)
            {
                return NotFound();
            }
            return View(meals);
        }

        // POST: Meals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MealsId,Name,Typeofday,Calories,Protein,Carbohydres,Fats")] Meals meals)
        {
            if (id != meals.MealsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    meals.UserId = (string)TempData["UserId"];
                    _context.Update(meals);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MealsExists(meals.MealsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = meals.MealsId });
            }
            return View(meals);
        }

        // GET: Meals/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            if (HttpContext.User.IsInRole("Admin"))
            {
                ViewBag.AbleToModify = true;
            }
            else
            {
                ViewBag.AbleToModify = false;
            }
            var meals = await _context.Meals
                .FirstOrDefaultAsync(m => m.MealsId == id);
            if (meals == null)
            {
                return NotFound();
            }

            return View(meals);
        }

        // POST: Meals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var meals = await _context.Meals.FindAsync(id);
            _context.Meals.Remove(meals);
            if (meals == null)
            {
                return NotFound();
            }
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MealsExists(int id)
        {
            return _context.Meals.Any(e => e.MealsId == id);
        }
    }
}
