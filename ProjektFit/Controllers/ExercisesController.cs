﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjektFit.Data;
using ProjektFit.Models;

namespace ProjektFit.Controllers
{
    [Authorize]
    public class ExercisesController : Controller
    {
        private readonly projectfitContext _context;
        private readonly ApplicationDbContext _contextUser;

        public ExercisesController(projectfitContext context,
            ApplicationDbContext contextUser)
        {
            _context = context;
            _contextUser = contextUser;
        }

        // GET: Exercises
        public async Task<IActionResult> Index()
        {
            if(HttpContext.User.IsInRole("Admin"))
            {
                //Getting all exercises
                var exercises = await _context.Exercise.ToListAsync();
                return View(exercises);
            }
            else
            {
                // Getting list of admins
                var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == "Admin");
                var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();
                List<Exercise> exercises = new List<Exercise>();
                //For each admin I find their exercises and then I add them to the exercises list
                foreach (var admin in admins)
                {
                    var tempAdminExercises = await _context.Exercise.Where(m => m.OwnerId == admin.UserId).ToListAsync();
                    exercises.AddRange(tempAdminExercises);
                }
                //Same with current user's exercises
                var tempUserExercises = await _context.Exercise.Where(m => m.OwnerId == currentUserId).ToListAsync();
                exercises.AddRange(tempUserExercises);
                return View(exercises);
            }
        }

        // GET: Exercises/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) 
            {
                return NotFound();
            }
            var exercise = await _context.Exercise
                .FirstOrDefaultAsync(m => m.ExerciseId == id);
            if (exercise == null) 
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var currentItemUserName = await _contextUser.Users
                .FirstOrDefaultAsync(m => m.Id == exercise.OwnerId);
            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == "Admin");
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();

            //Admin is the owner of the exercise
            if (admins.Find(m => m.UserId == exercise.OwnerId) != null)
            {
                ViewBag.Owner = "Admin";
                if (HttpContext.User.IsInRole("Admin"))
                {
                    ViewBag.AbleToModify = true;
                }
                else
                {
                    ViewBag.AbleToModify = false;
                }
                return View(exercise);
            }
            else
            {
                ViewBag.Owner = currentItemUserName.UserName;

                if (HttpContext.User.IsInRole("Admin") || exercise.OwnerId == currentUserId)
                {
                    ViewBag.AbleToModify = true;
                    return View(exercise);
                }
                else return NotFound();
            }
        }

        // GET: Exercises/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Exercises/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ExerciseId,Name,Url,Calories,OwnerId")] Exercise exercise)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                exercise.OwnerId = currentUserId;
                _context.Add(exercise);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(exercise);
        }

        // GET: Exercises/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var exercise = await _context.Exercise.FindAsync(id);
            if (exercise == null)
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == "Admin");
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();

            TempData["OwnerId"] = exercise.OwnerId;
            //Admin is the owner of the exercise
            if (admins.Find(m => m.UserId == exercise.OwnerId) != null)
            {
                if (HttpContext.User.IsInRole("Admin"))
                {
                    return View(exercise);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                if (HttpContext.User.IsInRole("Admin") || exercise.OwnerId == currentUserId)
                {
                    return View(exercise);
                }
                else return NotFound();
            }
        }

        // POST: Exercises/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ExerciseId,Name,Url,Calories,OwnerId")] Exercise exercise)
        {
            if (id != exercise.ExerciseId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    exercise.OwnerId = (string) TempData["OwnerId"];
                    _context.Update(exercise);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExerciseExists(exercise.ExerciseId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = exercise.ExerciseId});
            }
            return View(exercise);
        }

        // GET: Exercises/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exercise = await _context.Exercise
                .FirstOrDefaultAsync(m => m.ExerciseId == id);

            if (exercise == null)
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == "Admin");
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();

            //Admin is the owner of the exercise
            if (admins.Find(m => m.UserId == exercise.OwnerId) != null)
            {
                if (HttpContext.User.IsInRole("Admin"))
                {
                    return View(exercise);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                if (HttpContext.User.IsInRole("Admin") || exercise.OwnerId == currentUserId)
                {
                    return View(exercise);
                }
                else return NotFound();
            }
        }

        // POST: Exercises/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var exercise = await _context.Exercise.FindAsync(id);
            _context.Exercise.Remove(exercise);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExerciseExists(int id)
        {
            return _context.Exercise.Any(e => e.ExerciseId == id);
        }
    }
}
