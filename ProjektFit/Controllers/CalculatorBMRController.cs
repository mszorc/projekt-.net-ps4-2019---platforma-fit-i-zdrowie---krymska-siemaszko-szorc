﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjektFit.Models;
using static ProjektFit.Models.CalculatorBMR;

namespace ProjektFit.Controllers
{
    [Authorize]
    public class CalculatorBMRController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(CalculatorBMR model)
        {
            double BMR = (9.99 * model.Weight) + (6.25 * model.Height) - (4.92 * model.Age);
            if (model.Gender==Genderr.Female)
            {
                BMR = BMR - 161;
                if(model.Activity==Activityy.Negligible)
                {
                    BMR = BMR * 1.0;
                }
                else if (model.Activity == Activityy.VerySmall)
                {
                    BMR = BMR * 1.2;              
                }
                else if (model.Activity == Activityy.Little)
                {
                    BMR = BMR * 1.4;
                }
                else if (model.Activity == Activityy.Moderate)
                {
                    BMR = BMR * 1.6;
                }
                else if (model.Activity == Activityy.Big)
                {
                    BMR = BMR * 1.8;
                }
                else if (model.Activity == Activityy.VeryBig)
                {
                    BMR = BMR * 2.0;
                }

            }
            else
            {
                BMR = BMR+5;
                if (model.Activity == Activityy.Negligible)
                {
                    BMR = BMR * 1.0;
                }
                else if (model.Activity == Activityy.VerySmall)
                {
                    BMR = BMR * 1.2;
                }
                else if (model.Activity == Activityy.Little)
                {
                    BMR = BMR * 1.4;
                }
                else if (model.Activity == Activityy.Moderate)
                {
                    BMR = BMR * 1.6;
                }
                else if (model.Activity == Activityy.Big)
                {
                    BMR = BMR * 1.8;
                }
                else if (model.Activity == Activityy.VeryBig)
                {
                    BMR = BMR * 2.0;
                }
            }
            ViewBag.Result = BMR;
            return View();
        }
    }
}