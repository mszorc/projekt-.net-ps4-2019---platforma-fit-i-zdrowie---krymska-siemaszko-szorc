﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjektFit.Data;
using ProjektFit.Models;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Drawing;
using System.IO;
using Syncfusion.Pdf.Grid;
using Microsoft.AspNetCore.Hosting;

namespace ProjektFit.Controllers
{
    [Authorize]
    public class TrainingPlansController : Controller
    {
        private readonly projectfitContext _context;
        private readonly ApplicationDbContext _contextUser;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly string admin = "Admin";

        public TrainingPlansController(projectfitContext context,
            ApplicationDbContext contextUser,
            IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _contextUser = contextUser;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: TrainingPlans
        public async Task<IActionResult> Index()
        {
            if (HttpContext.User.IsInRole(admin))
            {
                var trainingplans = await _context.TrainingPlan.ToListAsync();
                return View(trainingplans);
            }
            else
            {
                var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == admin);
                var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();
                List<TrainingPlan> trainingplans = new List<TrainingPlan>();
                foreach (var admin in admins)
                {
                    var tempAdminTrainingPlans = await _context.TrainingPlan.Where(m => m.OwnerId == admin.UserId).ToListAsync();
                    trainingplans.AddRange(tempAdminTrainingPlans);
                }
                var tempUserTrainingPlan = await _context.TrainingPlan.Where(m => m.OwnerId == currentUserId).ToListAsync();
                trainingplans.AddRange(tempUserTrainingPlan);
                return View(trainingplans);
            }
        }

        // GET: TrainingPlans/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var trainingplan = await _context.TrainingPlan
                .FirstOrDefaultAsync(m => m.TrainingPlanId == id);
            if (trainingplan == null)
            {
                return NotFound();
            }

            var exercisetrainingplan = _context.ExerciseTrainingPlan.Where(m => m.TrainingPlanId == id);
            List<TrainingPlanPlusETPID> temp = new List<TrainingPlanPlusETPID>();
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var currentItemUserName = await _contextUser.Users
                .FirstOrDefaultAsync(m => m.Id == trainingplan.OwnerId);
            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == admin);
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();

            //Admin is the owner of training plan
            if (admins.Find(m => m.UserId == trainingplan.OwnerId) != null)
            {
                ViewBag.Owner = admin;
                if (HttpContext.User.IsInRole(admin))
                {
                    ViewBag.AbleToModify = true;
                }
                else
                {
                    ViewBag.AbleToModify = false;
                }
            }
            else
            {
                ViewBag.Owner = currentItemUserName.UserName;
                if (HttpContext.User.IsInRole(admin) || trainingplan.OwnerId == currentUserId)
                {
                    ViewBag.AbleToModify = true;
                }
                else
                {
                    return NotFound();
                }
            }

            foreach (var item in exercisetrainingplan)
            {
                var exercise = await _context.Exercise.FirstOrDefaultAsync(m => m.ExerciseId == item.ExerciseId);
                TrainingPlanPlusETPID x = new TrainingPlanPlusETPID(exercise, item);
                temp.Add(x);
            }
            ViewBag.ID = id;
            return View(temp);
        }

        // GET: TrainingPlans/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TrainingPlans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TrainingPlanId,Name,OwnerId")] TrainingPlan trainingplan)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                trainingplan.OwnerId = currentUserId;
                _context.Add(trainingplan);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(trainingplan);
        }

        // GET: TrainingPlans/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var trainingplan = await _context.TrainingPlan.FindAsync(id);
            if (trainingplan == null)
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == admin);
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();

            TempData["OwnerId"] = trainingplan.OwnerId;

            if (admins.Find(m => m.UserId == trainingplan.OwnerId) != null)
            {
                if (HttpContext.User.IsInRole(admin))
                {
                    return View(trainingplan);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                if (HttpContext.User.IsInRole(admin) || trainingplan.OwnerId == currentUserId)
                {
                    return View(trainingplan);
                }
                else return NotFound();
            }
        }

        // POST: TrainingPlans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TrainingPlanId,Name,OwnerId")] TrainingPlan trainingPlan)
        {
            if (id != trainingPlan.TrainingPlanId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    trainingPlan.OwnerId = (string)TempData["OwnerId"];
                    _context.Update(trainingPlan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrainingPlanExists(trainingPlan.TrainingPlanId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = trainingPlan.TrainingPlanId });
            }
            return View(trainingPlan);
        }

        // GET: TrainingPlans/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var trainingplan = await _context.TrainingPlan
                .FirstOrDefaultAsync(m => m.TrainingPlanId == id);
            if (trainingplan == null)
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == admin);
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();

            //Admin is the owner of training plan
            if (admins.Find(m => m.UserId == trainingplan.OwnerId) != null)
            {
                if (HttpContext.User.IsInRole(admin))
                {
                    return View(trainingplan);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                if (HttpContext.User.IsInRole(admin) || trainingplan.OwnerId == currentUserId)
                {
                    return View(trainingplan);
                }
                else return NotFound();
            }
        }

        // POST: TrainingPlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var trainingPlan = await _context.TrainingPlan.FindAsync(id);
            _context.TrainingPlan.Remove(trainingPlan);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrainingPlanExists(int id)
        {
            return _context.TrainingPlan.Any(e => e.TrainingPlanId == id);
        }


        // GET: TrainingPlans/AddExercise
        public async Task<IActionResult> AddExercise(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (TempData["ModelState"] != null)
            {
                ModelState.AddModelError(string.Empty, (string)TempData["ModelState"]);
            }

            ViewBag.ID = id;         

            var temp = await _context.TrainingPlan.FirstOrDefaultAsync(m => m.TrainingPlanId == id);
            if (temp == null)
            {
                return NotFound();
            }
            TempData["TrainingPlanId"] = temp.TrainingPlanId;

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == admin);
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();
            List<Exercise> exercises = new List<Exercise>();
            foreach (var admin in admins)
            {
                var tempAdminExercises = await _context.Exercise.Where(m => m.OwnerId == admin.UserId).ToListAsync();
                exercises.AddRange(tempAdminExercises);
            }

            if (HttpContext.User.IsInRole(admin))
            {  
                ViewData["ExerciseId"] = new SelectList(exercises, "ExerciseId", "Name");
            }
            else
            {
                var tempUserExercises = await _context.Exercise.Where(m => m.OwnerId == currentUserId).ToListAsync();
                exercises.AddRange(tempUserExercises);
                ViewData["ExerciseId"] = new SelectList(exercises, "ExerciseId", "Name");
            }
            return View();
        }

        // POST: ExerciseTrainingPlans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExercise([Bind("ExerciseId,TrainingPlanId,OwnerId")] ExerciseTrainingPlan exerciseTrainingPlan)
        {
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (ModelState.IsValid)
            {
                int trainingplanid = (int)TempData["TrainingPlanId"];

                var exerciseTrainingPlan2 = await _context.ExerciseTrainingPlan
                    .FirstOrDefaultAsync(m => m.TrainingPlanId == trainingplanid
                    && m.ExerciseId == exerciseTrainingPlan.ExerciseId);

                if (exerciseTrainingPlan2 != null)
                {
                    TempData["ModelState"] = "Exercise already exists in this plan.";
                    return RedirectToAction("AddExercise");
                }
                exerciseTrainingPlan.OwnerId = currentUserId;
                exerciseTrainingPlan.TrainingPlanId = trainingplanid;

                _context.Add(exerciseTrainingPlan);
                await _context.SaveChangesAsync();
                ModelState.Clear();
                return RedirectToAction("Details", new { id = exerciseTrainingPlan.TrainingPlanId});
            }
            /*

            ViewData["TrainingPlanId"] = new SelectList(_context.TrainingPlan.Where(m => m.TrainingPlanId == exerciseTrainingPlan.TrainingPlanId), "TrainingPlanId", "Name");

            if (ViewData["TrainingPlanId"] == null)
            {
                return NotFound();
            }

            if (HttpContext.User.IsInRole("Admin"))
            {
                ViewData["ExerciseId"] = new SelectList(_context.Exercise.Where(m => m.OwnerId == "Admin"), "ExerciseId", "Name");
            }
            else
            {
                ViewData["ExerciseId"] = new SelectList(_context.Exercise.Where(m => m.OwnerId == currentUserId || m.OwnerId == "Admin"), "ExerciseId", "Name");
            }*/
            return View(exerciseTrainingPlan);
        }

        // GET: ExerciseTrainingPlans/Delete/5
        public async Task<IActionResult> RemoveExercise(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exerciseTrainingPlan = await _context.ExerciseTrainingPlan
                .Include(e => e.Exercise)
                .Include(e => e.TrainingPlan)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (exerciseTrainingPlan == null)
            {
                return NotFound();
            }

            var TrainingPlan = await _context.TrainingPlan
                .FirstOrDefaultAsync(m => m.TrainingPlanId == exerciseTrainingPlan.TrainingPlanId);

            if (TrainingPlan == null)
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == admin);
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();

            if (admins.Find(m => m.UserId == TrainingPlan.OwnerId) != null)
            {
                if (HttpContext.User.IsInRole(admin))
                {
                    return View(exerciseTrainingPlan);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                if (HttpContext.User.IsInRole(admin) || TrainingPlan.OwnerId == currentUserId)
                {
                    return View(exerciseTrainingPlan);
                }
                else return NotFound();
            }
        }

        // POST: ExerciseTrainingPlans/Delete/5
        [HttpPost, ActionName("RemoveExercise")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveExerciseConfirmed(int id)
        {
            var exerciseTrainingPlan = await _context.ExerciseTrainingPlan.FindAsync(id);
            _context.ExerciseTrainingPlan.Remove(exerciseTrainingPlan);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", new { id = exerciseTrainingPlan.TrainingPlanId });
        }

        private bool ExerciseTrainingPlanExists(int id)
        {
            return _context.ExerciseTrainingPlan.Any(e => e.Id == id);
        }

        public async Task<IActionResult> ExportToPDF(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trainingplan = await _context.TrainingPlan
                .FirstOrDefaultAsync(m => m.TrainingPlanId == id);
            if (trainingplan == null)
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var currentItemUserName = await _contextUser.Users
                .FirstOrDefaultAsync(m => m.Id == trainingplan.OwnerId);
            var adminRoleId = await _contextUser.Roles.FirstOrDefaultAsync(m => m.Name == "Admin");
            var admins = await _contextUser.UserRoles.Where(m => m.RoleId == adminRoleId.Id).ToListAsync();
            
            if (admins.Find(m => m.UserId == trainingplan.OwnerId) == null)
            {
                if (!HttpContext.User.IsInRole("Admin") && trainingplan.OwnerId != currentUserId)
                {
                    return NotFound();
                }
            }

            var exercisetrainingplan = _context.ExerciseTrainingPlan.Where(m => m.TrainingPlanId == id);

            foreach (var item in exercisetrainingplan)
            {
                var exercise = await _context.Exercise.FirstOrDefaultAsync(m => m.ExerciseId == item.ExerciseId);
                TrainingPlanPlusETPID x = new TrainingPlanPlusETPID(exercise, item);
            }

            //Create a new PDF document.
            PdfDocument doc = new PdfDocument();
            //Add a page.
            PdfPage page = doc.Pages.Add();
            //Create PDF graphics for the page
            PdfGraphics graphics = page.Graphics;
            //Create a PdfGrid.
            PdfGrid pdfGrid = new PdfGrid();

            string path = _hostingEnvironment.WebRootPath + "/Font/Arial.ttf";

            Stream fontStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            //Create a new PDF true type font.
            PdfTrueTypeFont font = new PdfTrueTypeFont(fontStream, 12, PdfFontStyle.Regular);

            //Add values to list
            List<object> data = new List<object>();
            foreach(var item in exercisetrainingplan)
            {
                var exercise = await _context.Exercise.FirstOrDefaultAsync(m => m.ExerciseId == item.ExerciseId);
                Object row = new { Name = exercise.Name, Calories = exercise.Calories };
                data.Add(row);
            }
            //Add list to IEnumerable
            IEnumerable<object> dataTable = data;
            //Assign data source.
            pdfGrid.DataSource = dataTable;
            pdfGrid.Headers[0].Style.Font = font;
            pdfGrid.Style.Font = font;
            //Draw grid to the page of PDF document.
            pdfGrid.Draw(page, new Syncfusion.Drawing.PointF(10, 10));
            //Save the PDF document to stream
            MemoryStream stream = new MemoryStream();
            doc.Save(stream);
            //If the position is not set to '0' then the PDF will be empty.
            stream.Position = 0;
            //Close the document.
            doc.Close(true);
            //Defining the ContentType for pdf file.
            string contentType = "application/pdf";
            //Define the file name.
            string fileName = "TrainingPlan.pdf";
            fontStream.Close();
            //Creates a FileContentResult object by using the file contents, content type, and file name.
            return File(stream, contentType, fileName);
        }
    }
}
