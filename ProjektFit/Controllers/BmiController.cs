﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjektFit.Models;
using static ProjektFit.Models.BmiModel;

namespace ProjektFit.Controllers
{
    [Authorize]
    public class BmiController : Controller
    {
        
        [HttpGet]
        public IActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        public IActionResult bmi(BmiModel model)
        {

            string Wiadomosc="";
            double bmiliczba;

            //bmiliczba = (model.Waga / (model.Wzrost * model.Wzrost))/10000;
            int waga = model.Waga;
            int wzrost = model.Wzrost;
            wzrost = wzrost * wzrost;
            ViewBag.Resulttrzy = wzrost;
            waga = (waga*10000);
            ViewBag.Resultcztery = waga;
            bmiliczba = waga / wzrost;
            ViewBag.Resultpiec = bmiliczba;

            if (model.Wzrost != 0 && model.Waga != 0  )
            {
                if (bmiliczba <= 16)
                {
                    Wiadomosc = "wygłodzenie";
                }
                if (bmiliczba > 16 && bmiliczba <= 17)
                {
                    Wiadomosc = "wychudzenie";
                }
                if (bmiliczba <= 18.5 && bmiliczba > 17)
                {
                    Wiadomosc = "niedowaga";
                }
                if (bmiliczba <= 25 && bmiliczba > 18.5)
                {
                    Wiadomosc = "wartość prawidłowa";
                }
                if (bmiliczba <= 30 && bmiliczba > 25)
                {
                    Wiadomosc = "nadwaga";
                }
                if (bmiliczba <= 35 && bmiliczba > 30)
                {
                    Wiadomosc = "I stopień otyłości";
                }
                if (bmiliczba <= 40 && bmiliczba > 35)
                {
                    Wiadomosc = "II stopień otyłości";
                }
                if (bmiliczba > 40)
                {
                    Wiadomosc = "otyłość skrajna";
                }
           }

            //else
            //{
            //   Wiadomosc = "Wprowadź dane";
            //}

            ViewBag.Resultdwa = bmiliczba;
            ViewBag.Result = Wiadomosc;
            return View("Index");
        }
    }
}
