﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjektFit.Models;
using ProjektFit.Data;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.AspNetCore.Authorization;

namespace ProjektFit.Controllers
{
    [Authorize]
    public class ChartsController : Controller
    {
        private readonly ChartsContext _context;

        public ChartsController(ChartsContext context)
        {
            _context = context;
        }

        // GET: Charts
        public async Task<IActionResult> Index()
        {
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var query = await _context.Charts.Where(m => m.UserId == currentUserId).ToListAsync();

            return View(query);
        }

        // GET: Charts/Create
        public IActionResult Create()
        {

            return View();

        }

        // POST: Charts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ChartsId,UserId,Waga,ObwodWKlatcePiersiowej,ObwodWPasie")] Charts charts)
        {
            if (ModelState.IsValid)
            {
                charts.UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                _context.Add(charts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(charts);
        }

        // GET: Charts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var charts = await _context.Charts.FindAsync(id);
            if (charts == null)
            {
                return NotFound();
            }
            TempData["OwnerId"] = charts.UserId;
            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (currentUserId == charts.UserId)
            {
                return View(charts);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: Charts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ChartsId,UserId,Waga,ObwodWKlatcePiersiowej,ObwodWPasie")] Charts charts)
        {
            if (id != charts.ChartsId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    charts.UserId = (string)TempData["OwnerId"];
                    _context.Update(charts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ChartsExists(charts.ChartsId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(charts);
        }

        // GET: Charts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var charts = await _context.Charts
                .FirstOrDefaultAsync(m => m.ChartsId == id);
            if (charts == null)
            {
                return NotFound();
            }

            var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (currentUserId == charts.UserId)
            {
                return View(charts);
            }
            else
            {
                return NotFound();
            }
        }


        public IActionResult Expense([Bind("ChartsId,UserId,Waga,ObwodWKlatcePiersiowej,ObwodWPasie")] Charts charts)
        {
       
            return View();
        }

        // POST: Charts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var charts = await _context.Charts.FindAsync(id);
            _context.Charts.Remove(charts);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ChartsExists(int id)
        {
            return _context.Charts.Any(e => e.ChartsId == id);
        }
    }
}
