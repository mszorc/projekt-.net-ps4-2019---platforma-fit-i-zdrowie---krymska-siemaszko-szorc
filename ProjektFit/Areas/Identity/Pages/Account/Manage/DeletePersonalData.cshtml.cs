﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ProjektFit.Models;

namespace ProjektFit.Areas.Identity.Pages.Account.Manage
{
    public class DeletePersonalDataModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<DeletePersonalDataModel> _logger;
        private readonly projectfitContext _contextExercise;
        private readonly MealsContext _contextMeals;
        private readonly GroupsContext _contextGroups;
        private readonly ChartsContext _contextCharts;


        public DeletePersonalDataModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<DeletePersonalDataModel> logger,
            projectfitContext contextExercise,
            MealsContext contextMeals,
            GroupsContext contextGroups,
            ChartsContext contextCharts)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _contextExercise = contextExercise;
            _contextMeals = contextMeals;
            _contextGroups = contextGroups;
            _contextCharts = contextCharts;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }

        public bool RequirePassword { get; set; }

        public async Task<IActionResult> OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            RequirePassword = await _userManager.HasPasswordAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            RequirePassword = await _userManager.HasPasswordAsync(user);
            if (RequirePassword)
            {
                if (!await _userManager.CheckPasswordAsync(user, Input.Password))
                {
                    ModelState.AddModelError(string.Empty, "Password not correct.");
                    return Page();
                }
            }

            _contextExercise.Exercise.Where(m => m.OwnerId == user.Id).ToList().ForEach(m => _contextExercise.Exercise.Remove(m));
            _contextExercise.SaveChanges();
            _contextExercise.TrainingPlan.Where(m => m.OwnerId == user.Id).ToList().ForEach(m => _contextExercise.TrainingPlan.Remove(m));
            _contextExercise.SaveChanges();

            _contextGroups.Group.Where(m => m.OwnerGroupId == user.Id).ToList().ForEach(m => _contextGroups.Group.Remove(m));
            _contextGroups.SaveChanges();
            _contextGroups.GroupUser.Where(m => m.UserId == user.Id).ToList().ForEach(m => _contextGroups.GroupUser.Remove(m));
            _contextGroups.SaveChanges();

            _contextCharts.Charts.Where(m => m.UserId == user.Id).ToList().ForEach(m => _contextCharts.Charts.Remove(m));
            _contextCharts.SaveChanges();

            var result = await _userManager.DeleteAsync(user);
            var userId = await _userManager.GetUserIdAsync(user);
            if (!result.Succeeded)
            {
                throw new InvalidOperationException($"Unexpected error occurred deleteing user with ID '{userId}'.");
            }

            await _signInManager.SignOutAsync();

            _logger.LogInformation("User with ID '{UserId}' deleted themselves.", userId);

            return Redirect("~/");
        }
    }
}